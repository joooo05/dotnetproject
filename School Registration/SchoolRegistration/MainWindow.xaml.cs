﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchoolRegistration
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Course> CourseList = new List<Course>();

        public MainWindow()
        {
            InitializeComponent();
            lvCourse.ItemsSource = CourseList;

            try
            {
             Globals.Db = new Database(); 
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Todos Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        private void ReloadList()
        {
            try
            {
                List<Course> list = Globals.Db.GetAllCourse();
                CourseList.Clear();
                foreach (Course c in list)
                {
                    CourseList.Add(c);
                }
                lvCourse.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error: executing SQL query:\n" + ex.Message, "DotNetProject Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void FilePrint_MenuClick(object sender, RoutedEventArgs e)
        {
            PrintDialog dlg = new PrintDialog();

            Window currentMainWindow = Application.Current.MainWindow;

            Application.Current.MainWindow = this;

            if ((bool)dlg.ShowDialog().GetValueOrDefault())
            {
                Application.Current.MainWindow = currentMainWindow; // do it early enough if the 'if' is entered
                dlg.PrintVisual(this, "Certificate");
            }
            /*
            // Create the print dialog object and set options
            PrintDialog pDialog = new PrintDialog();
            pDialog.PageRangeSelection = PageRangeSelection.AllPages;
            pDialog.UserPageRangeEnabled = true;

            // Display the dialog. This returns true if the user presses the Print button.
            Nullable<Boolean> print = pDialog.ShowDialog();
            if (print == true)
            {
                XpsDocument xpsDocument = new XpsDocument("C:\\FixedDocumentSequence.xps", FileAccess.ReadWrite);
                FixedDocumentSequence fixedDocSeq = xpsDocument.GetFixedDocumentSequence();
                pDialog.PrintDocument(fixedDocSeq.DocumentPaginator, "Test print job");
            }
            */
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddCourse_MenuClick(object sender, RoutedEventArgs e)
        {
            AddNewCourses addNewCourse = new AddNewCourses();
            addNewCourse.Show();
        }

        private void AddKlasse_MenuClick(object sender, RoutedEventArgs e)
        {
            AddNewKlasses addNewKlasses = new AddNewKlasses();
            addNewKlasses.Show();
        }

        private void StudentRegistration_MenuClick(object sender, RoutedEventArgs e)
        {
            AddStudent addStudent = new AddStudent();
            addStudent.Show();
        }

        private void Calendar_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CalendarStudent calendar = new CalendarStudent();
                calendar.Show();
        }

        private void Login_MenuClick(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.Show();
        }

        private void Logout_MenuClick(object sender, RoutedEventArgs e)
        {

        }
    }  
}
