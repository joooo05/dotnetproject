﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolRegistration
{
    /// <summary>
    /// Interaction logic for AddStudent.xaml
    /// </summary>
    public partial class AddStudent : Window
    {
         bool hasUnsavedData= false;

        public AddStudent()
        {
            InitializeComponent();
            Globals.Db.BindComboBoxStudent(cbCourse);
        }

       

        private void ButtonNew_Click(object sender, RoutedEventArgs e)
        {
            tbFirstname.Text = "";
            tbLastname.Text = "";
            tbMothername.Text = "";
            tbID.Text = "";

            tbPostalCode.Text = "";

        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {

            string fname = tbFirstname.Text;
            if (!Regex.IsMatch(fname, @"^[^;]{1,30}$"))
            {
                MessageBox.Show("First Name invalid, must be 2-30 characters, no semicolons");
                return;
            }
            string lname = tbLastname.Text;
            if (!Regex.IsMatch(lname, @"^[^;]{1,}$"))
            {
                MessageBox.Show("Last Name invalid, must be not empty, no semicolons");
                return;
            }
            string mname = tbMothername.Text;
            if (!Regex.IsMatch(mname, @"^[^;]{1,}$"))
            {
                MessageBox.Show("Mother Name invalid, must be not empty, no semicolons");
                return;
            }
            string tbid = tbID.Text;
            double idd;
            if (!double.TryParse(tbid, out idd))
            {
                MessageBox.Show("Invalid input: ID must be numerical");
                return;
            }

            string Male = "";
            string FeMale = "";
            if (rbmale.IsChecked == true)
            {
                Male = "Male";
            }
            
             if (rbfemale.IsChecked == true)
            {
                FeMale = "Female";
            }
            else
            {
                // Internal Error
                MessageBox.Show("You Are Male or Female???");
                return;
            }
            string course = cbCourse.Text;
            if (course == "")
            {
                MessageBox.Show("Select a Course", "Course", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            hasUnsavedData = true;
            MessageBox.Show("Data saved");

        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonCamera_Click(object sender, RoutedEventArgs e)
        {
            Webcam webcam = new Webcam();
            try
            {
                webcam.Show();
            }
            catch (InvalidOperationException)
            {
                return;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (hasUnsavedData)
            {
                MessageBoxResult result = MessageBox.Show("Unsaved data. Are you sure  you want to exit?", "Students Name", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
                if (result == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }




        private void BSave_Click(object sender, RoutedEventArgs e)
        {

            string fname = tbFirstname.Text;
            if (!Regex.IsMatch(fname, @"^[^;]{2,30}$"))
            {
                MessageBox.Show("Name invalid, must be 2-30 characters, no semicolons");
                return;
            }
            string lname = tbLastname.Text;
            if (!Regex.IsMatch(lname, @"^[^;]{2,30}$"))
            {
                MessageBox.Show("Name invalid, must be 2-30 characters, no semicolons");
                return;
            }
            string mname = tbMothername.Text;
            if (!Regex.IsMatch(mname, @"^[^;]{2,30}$"))
            {
                MessageBox.Show("Name invalid, must be 2-30 characters, no semicolons");
                return;
            }
            string idStr = tbID.Text;
            int id;
            if ((!int.TryParse(idStr, out id)) || id < 100 || id > 5000)
            {
                MessageBox.Show("Age invalid, must be an integer between 100-5000");
                return;
            }
        
            string gender = "";
            if (rbmale.IsChecked == true)
            {
                gender = "Male";
            }
            else if (rbfemale.IsChecked == true)
            {
                gender = "Female";
            }
            else
            {
                // Internal Error
                MessageBox.Show("Please Select Male or Female");
                return;
            }
            MessageBox.Show("Data saved");
        }
    }
}
