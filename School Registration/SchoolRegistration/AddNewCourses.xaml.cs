﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolRegistration
{
    /// <summary>
    /// Interaction logic for AddNewCourses.xaml
    /// </summary>
    public partial class AddNewCourses : Window
    {
        Course EditCourse;

        public AddNewCourses()
        {
            InitializeComponent();
            Globals.Db.BindComboBoxUser(cbxTeacherID);
        }

        public AddNewCourses(Window owner, Course editCourse = null)
        {
            InitializeComponent();
            Owner = owner;
            EditCourse = editCourse;
        }

        private void ButtonAddCourse_Click(object sender, RoutedEventArgs e)
        {
            string semester = cbxSemester.Text;
            string courseName = tbCourseName.Text;
            string courseDescription = tbCourseDescription.Text;
            string teacherIdStr = cbxTeacherID.Text;

            if (!int.TryParse(teacherIdStr, out int teacherId))
            {
                MessageBox.Show("Error");
            }

            if (EditCourse == null)
            {
                Course course = new Course() { Semester = semester, CourseName = courseName, Description = courseDescription, TeacherId = teacherId};
                try
                {
                    Globals.Db.AddCourse(course);
                }
                catch(SqlException ex)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                        "DotNetProject Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                
            }
            AddNewKlasses addNewKlasses = new AddNewKlasses();
            addNewKlasses.Show();

        }

        private void ButtonCancelAddNewCourse_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you don't want to add the new course?", "Add New Course", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                Close();
            }
        }
    }
}
