﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolRegistration
{
    /// <summary>
    /// Interaction logic for AddNewKlasses.xaml
    /// </summary>
    public partial class AddNewKlasses : Window
    {
        Klass EditKlass;
        List<Klass> KlassList = new List<Klass>();

        public AddNewKlasses()
        {
            InitializeComponent();
            lvKlasses.ItemsSource = KlassList;
            Globals.Db.BindComboBoxCourse(cbxCourseId);
            ReloadListKlass();
        }

        private void ReloadListKlass()
        {
            try
            {
                List<Klass> list = Globals.Db.GetAllKlass();
                KlassList.Clear();
                foreach (Klass k in list)
                {
                    KlassList.Add(k);
                }
                lvKlasses.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error: executing SQL query:\n" + ex.Message, "DotNetProject Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public AddNewKlasses(Window owner, Klass editKlass = null)
        {
            InitializeComponent();
            Owner = owner;
            EditKlass = editKlass;
        }

        private void ButtonAddKlass_Click(object sender, RoutedEventArgs e)
        {
            string courseIdStr = cbxCourseId.Text;
            string klassName = tbKlassName.Text;
            string dateStartStr = dpDateStart.Text;
            string dateEndStr = dpDateEnd.Text;

            if (!int.TryParse(courseIdStr, out int courseId))
            {
                MessageBox.Show("Error");
            }

            if (!DateTime.TryParse(dateStartStr, out DateTime dateStart))
            {
                MessageBox.Show("Error");
            }

            if (!DateTime.TryParse(dateEndStr, out DateTime dateEnd))
            {
                MessageBox.Show("Error");
            }

            if (EditKlass == null)
            {
                Klass klass = new Klass() { CourseId = courseId, KlasseName = klassName, DateStart = dateStart, DateEnd = dateEnd };
                try
                {
                    Globals.Db.AddKlass(klass);
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                        "DotNetProject Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }


            }
                ReloadListKlass();


        }

        private void ButtonCancelAddNewKlass_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you don't want to add the new class or classes?", "Add New Class", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                Close();
            }
        }
        
    }
}
