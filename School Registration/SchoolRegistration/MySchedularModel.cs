namespace SchoolRegistration
{
    using System;
    //using System.Data.Entity;
    using System.Linq;
    using System.Windows.Input;
    using System.ComponentModel.DataAnnotations;
    using System.Collections.Generic;

    public class MySchedularModel //: DbContext
    {
       /* // Your context has been configured to use a 'MySchedularModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'SchoolRegistration.MySchedularModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'MySchedularModel' 
        // connection string in the application configuration file.
        public MySchedularModel()
            : base("name=MySchedularModel")
        {
            Database.SetInitializer<MySchedularModel>(new SchedulerDBInitializer());
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }
    public class SchedulerDBInitializer : CreateDatabaseIfNotExists<MySchedularModel>
    {
        Protected Overrides void Seed(MySchedularModel context)
        {
         IList<EFResource> = new List<EFResource>();

            defaultResources.Add(new EFResource() {.ResourceID = 1, ResourceName = "Resource 1" });
            defaultResources.Add(new EFResource() {.ResourceID = 2, ResourceName = "Resource 2" });

        Foreach (EFResource In defaultResources)
            context.MyResources.Add(res)
        base.Seed(context);
    }*/

}
/* public class EFAppointment
        {
     [Key()]
     public int UniqueID { get; set; }
     public int Type() { get; set; } _
     public int StartDate() { get; set; }
     <Required> _


     public int EndDate() { get; set; }
        public bool AllDay() { get; set; }
        public string Subject() { get; set; }
        public string Location() { get; set; }
        public string Description() { get; set; }
        public int Status() { get; set; }
        public int Label() { get; set; }
        public string ResourceIDs() { get; set; }
        public string ReminderInfo() { get; set; }
        public string RecurrenceInfo() { get; set; }
        End Class
      }

 public class EFResource
    <Key()> _
     public int UniqueID() As Integer
     public int ResourceID() As Integer
     public int ResourceName() As String
     public int Color() As Integer
 End Class*/
}