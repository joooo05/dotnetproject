﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SchoolRegistration
{
    class Database
    {
        const string DbConnectionString = @"Server=tcp:ipd16-schoolregistrationcalendar.database.windows.net,1433;Initial Catalog=DotNetProject;Persist Security Info=False;User ID = sqladmin; Password = Sqlipd16;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(DbConnectionString); 
            conn.Open();
        }

        public void BindComboBoxUser(ComboBox cbxTeacherId)
        {
            SqlDataAdapter da = new SqlDataAdapter("Select UserId FROM Users WHERE Role LIKE 'Teacher'", conn); //where role = teacher 
            DataSet ds = new DataSet();
            da.Fill(ds, "Users");
            cbxTeacherId.ItemsSource = ds.Tables[0].DefaultView;
            cbxTeacherId.DisplayMemberPath = ds.Tables[0].Columns["UserId"].ToString();
            cbxTeacherId.SelectedValuePath = ds.Tables[0].Columns["UserId"].ToString();
        }

            public void BindComboBoxCourse(ComboBox cbxCourseId)
        {
            SqlDataAdapter da = new SqlDataAdapter("Select CourseId FROM Courses", conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "Users");
            cbxCourseId.ItemsSource = ds.Tables[0].DefaultView;
            cbxCourseId.DisplayMemberPath = ds.Tables[0].Columns["CourseId"].ToString();
            cbxCourseId.SelectedValuePath = ds.Tables[0].Columns["CourseId"].ToString();
        }

        public void BindComboBoxStudent(ComboBox cbCourse)
        {
            SqlDataAdapter da = new SqlDataAdapter("Select CourseName FROM Courses", conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "Courses");
            cbCourse.ItemsSource = ds.Tables[0].DefaultView;
            cbCourse.DisplayMemberPath = ds.Tables[0].Columns["CourseName"].ToString();
            cbCourse.SelectedValuePath = ds.Tables[0].Columns["CourseName"].ToString();
        }

        public List<Course> GetAllCourse()
        {
            List<Course> list = new List<Course>();
            SqlCommand cmdSelect = new SqlCommand("SELECT c.CourseId, c.TeacherId, c.Semester, c.CourseName, c.Description FROM Courses AS c", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    int teacherid = (int)reader[1];
                    string courseName = (string)reader[2];
                    string semester = (string)reader[3];
                    string description = (string)reader[4];
                    list.Add(new Course() { CourseId = id, TeacherId = teacherid, CourseName = courseName, Semester = semester, Description = description });
                }
            }
            return list;
        }

        public int AddCourse(Course course)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Courses(TeacherId, Semester, CourseName, Description) OUTPUT INSERTED.CourseId VALUES(@TeacherId, @Semester, @CourseName, @Description)", conn);
            cmdInsert.Parameters.AddWithValue("CourseId", course.CourseId);
            cmdInsert.Parameters.AddWithValue("TeacherId", course.TeacherId);
            cmdInsert.Parameters.AddWithValue("Semester", course.Semester);
            cmdInsert.Parameters.AddWithValue("CourseName", course.CourseName);
            cmdInsert.Parameters.AddWithValue("Description", course.Description);
            int insertId = (int)cmdInsert.ExecuteScalar();
            return insertId;
        }

        public List<Klass> GetAllKlass()
        {
            List<Klass> list = new List<Klass>();
            SqlCommand cmdSelect = new SqlCommand("SELECT k.KlasseId, k.CourseId, k.KlasseName, k.DateStart, k.DateEnd FROM Klasses AS k", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    int courseid = (int)reader[1];
                    string klassName = (string)reader[2];
                    DateTime dateStart = (DateTime)reader[3];
                    DateTime dateEnd = (DateTime)reader[4];
                    list.Add(new Klass() { KlasseId = id, CourseId = courseid, KlasseName = klassName, DateStart = dateStart, DateEnd = dateEnd});
                }
            }
            return list;
        }

        public int AddKlass(Klass klass)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Klasses(CourseId, KlasseName, DateStart, DateEnd) OUTPUT INSERTED.KlasseId VALUES(@CourseId, @KlasseName, @DateStart, @DateEnd)", conn);
            cmdInsert.Parameters.AddWithValue("KlasseId", klass.KlasseId);
            cmdInsert.Parameters.AddWithValue("CourseId", klass.CourseId);
            cmdInsert.Parameters.AddWithValue("KlasseName", klass.KlasseName);
            cmdInsert.Parameters.AddWithValue("DateStart", klass.DateStart);
            cmdInsert.Parameters.AddWithValue("DateEnd", klass.DateEnd);
            int insertId = (int)cmdInsert.ExecuteScalar();
            return insertId;
        }

        public int AddUser(User user)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Users(FirstName, LastName, Password, Role, Birthday, PhoneNumber, Street, City, PostalCode, Province, Photo) OUTPUT INSERTED.UserId VALUES(@FirstName, @LastName, @Password, @Role, @Birthday, @PhoneNumber, @Street @City, @PostalCode, @Province, @Photo)", conn);
            cmdInsert.Parameters.AddWithValue("UserId", user.UserId);
            cmdInsert.Parameters.AddWithValue("FirstName", user.FirstName);
            cmdInsert.Parameters.AddWithValue("LastName", user.LastName);
            cmdInsert.Parameters.AddWithValue("Password", user.Password);
            cmdInsert.Parameters.AddWithValue("Role", user.Role);
            cmdInsert.Parameters.AddWithValue("Birthday", user.Birthday);
            cmdInsert.Parameters.AddWithValue("PhoneNumber", user.PhoneNumber);
            cmdInsert.Parameters.AddWithValue("Street", user.Street);
            cmdInsert.Parameters.AddWithValue("City", user.City);
            cmdInsert.Parameters.AddWithValue("PostalCode", user.PostalCode);
            cmdInsert.Parameters.AddWithValue("Province", user.Province);
            cmdInsert.Parameters.AddWithValue("Photo", user.Photo);
            cmdInsert.Parameters.AddWithValue("Email", user.Email);
            int insertId = (int)cmdInsert.ExecuteScalar();
            return insertId;
        }

        public List<User> GetUserStudentInfo(User user, string tbEmailStudent, string tbPasswordStudent)
        {
            List<User> list = new List<User>();
            SqlCommand cmdSelect = new SqlCommand ("SELECT u.Email, u.Password FROM Users AS u WHERE Role LIKE 'Student'", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    string password = (string)reader[0];
                    string email = (string)reader[1];
                    list.Add(new User() { Email = email, Password = password });
                }
            }
            return list;
        }

        public List<User> GetUserTeacherInfo(User user, string tbEmailTeacher, string tbPasswordTeacher)
        {
            List<User> list = new List<User>();
            SqlCommand cmdSelect = new SqlCommand("SELECT u.Email, u.Password FROM Users AS u WHERE Role LIKE 'Teacher'", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    string password = (string)reader[0];
                    string email = (string)reader[1];
                    list.Add(new User() { Email = email, Password = password });
                }
            }
            return list;
        }
    }   
}
