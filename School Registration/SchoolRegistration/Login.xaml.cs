﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolRegistration
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
        }

        private void ButtonLogInStudent_Click(object sender, RoutedEventArgs e)
        {
            string email = tbEmailStudent.Text;
            string password = tbPasswordStudent.Text;

            if (email.Length < 1 && password.Length < 1)
            {
                MessageBox.Show("Please enter your email and password!", "Login failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (email.Length < 1)
            {
                MessageBox.Show("Please enter your email!", "Login failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (password.Length < 1)
            {
                MessageBox.Show("Please enter your password !", "Login failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show("Error to login! please try again!",
                        "Login failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }


            try
            {
                User user = new User() { Email = email, Password = password };
                List<User> list = Globals.Db.GetUserStudentInfo(user, tbEmailStudent.Text, tbPasswordStudent.Text);



                if (user.Email == tbEmailStudent.Text && user.Password == tbPasswordStudent.Text)
                {
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                }
                else
                {
                    MessageBox.Show("User Name and Password are Not Match !", "Error"); return;
                }
            }
            catch(SqlException ex)
            {
                MessageBox.Show("Error: executing SQL query:\n" + ex.Message, "DotNetProject Database", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void ButtonLogInTeacher_Click(object sender, RoutedEventArgs e)
        {
            string email = tbEmailTeacher.Text;
            string password = tbPasswordTeacher.Text;

            if (email.Length < 1 && password.Length < 1)
            {
                MessageBox.Show("Please enter your email and password!", "Login failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (email.Length < 1)
            {
                MessageBox.Show("Please enter your email!", "Login failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (password.Length < 1)
            {
                MessageBox.Show("Please enter your password !", "Login failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show("Error to login! please try again!",
                        "Login failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }


            try
            {
                User user = new User() { Email = email, Password = password };
                List<User> list = Globals.Db.GetUserTeacherInfo(user, tbEmailTeacher.Text, tbPasswordTeacher.Text);



                if (user.Email == tbEmailTeacher.Text && user.Password == tbPasswordTeacher.Text)
                {
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.Show();
                }
                else
                {
                    MessageBox.Show("User Name and Password are Not Match !", "Error"); return;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error: executing SQL query:\n" + ex.Message, "DotNetProject Database", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }
    }
}
