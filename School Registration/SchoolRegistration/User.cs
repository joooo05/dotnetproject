﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolRegistration
{
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public DateTime Birthday { get; set; }
        public char PhoneNumber { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public char PostalCode { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string Email{ get; set; }
        public string Gender { get; set; }
        public byte Photo { get; set; }
    }
}
