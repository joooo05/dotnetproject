﻿
using Microsoft.Expression.Encoder.Devices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolRegistration
{
    /// <summary>
    /// Interaction logic for Webcam.xaml
    /// </summary>
    public partial class Webcam : Window
    {
        public Collection<EncoderDevice> VideoDevices { get; set; }

        public Webcam()
        {
            InitializeComponent();
            this.DataContext = this;

           // VideoDevices = EncoderDevices.FindDevices(EncoderDeviceType.Video);    


            try
            {
                VideoDevices = EncoderDevices.FindDevices(EncoderDeviceType.Video);
               // AudioDevices = EncoderDevices.FindDevices(EncoderDeviceType.Audio);
            }
            catch(FileNotFoundException ex)
            {
                MessageBox.Show("Error cannot find device" + ex.Message, "Webcam Not found", MessageBoxButton.OK, MessageBoxImage.Warning);
                Close();
            }



        }
        private void StartCaptureButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Display webcam video
                WebcamViewer.StartPreview();
            }
            catch (Microsoft.Expression.Encoder.SystemErrorException ex)
            {
                MessageBox.Show("Device is in use by another application");
            }
        }
        private void StopCaptureButton_Click(object sender, RoutedEventArgs e)
        {
            // Stop the display of webcam video.
            WebcamViewer.StopPreview();
        }
        private void TakeSnapshotButton_Click(object sender, RoutedEventArgs e)
        {
            // Take snapshot of webcam video.
            WebcamViewer.TakeSnapshot();
            //WebCam webcam = new WebCam();
            //passing Image Control Element
            //webcam.InitializeWebCam(ref imgVideo);
            //start webcam video playing
           // webcam.start();
            //stop or pause video playing
           // webcam.stop();
            //continue playing after stop state
            //webcam.continue();

        }
    }
}