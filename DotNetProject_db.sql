USE [DotNetProject]
GO
/****** Object:  Table [dbo].[Courses]    Script Date: 5/10/2019 2:00:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Courses](
	[CourseId] [int] IDENTITY(1,1) NOT NULL,
	[TeacherID] [int] NOT NULL,
	[CourseName] [nvarchar](50) NOT NULL,
	[Semester] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](250) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CourseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Klasses]    Script Date: 5/10/2019 2:00:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Klasses](
	[KlasseId] [int] IDENTITY(1,1) NOT NULL,
	[CourseID] [int] NOT NULL,
	[KlasseName] [nvarchar](50) NOT NULL,
	[DateStart] [date] NOT NULL,
	[DateEnd] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[KlasseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Registerations]    Script Date: 5/10/2019 2:00:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registerations](
	[RegistrationId] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RegistrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 5/10/2019 2:00:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](15) NOT NULL,
	[Role] [nvarchar](8) NOT NULL,
	[Birthday] [date] NOT NULL,
	[PhoneNumber] [nchar](10) NOT NULL,
	[Street] [nvarchar](50) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[PostalCode] [nchar](6) NOT NULL,
	[Province] [nvarchar](50) NOT NULL,
	[Photo] [varbinary](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Courses] ON 

INSERT [dbo].[Courses] ([CourseId], [TeacherID], [CourseName], [Semester], [Description]) VALUES (13, 13, N'French', N'Winter', N'In this class you will learn the basics of the french language!')
INSERT [dbo].[Courses] ([CourseId], [TeacherID], [CourseName], [Semester], [Description]) VALUES (40, 13, N'English', N'Summer', N'You will learn the basics of english!')
INSERT [dbo].[Courses] ([CourseId], [TeacherID], [CourseName], [Semester], [Description]) VALUES (41, 15, N'CSS', N'Fall', N'You will learn how to improve your HTML web pages by making them prettier!')
SET IDENTITY_INSERT [dbo].[Courses] OFF
SET IDENTITY_INSERT [dbo].[Klasses] ON 

INSERT [dbo].[Klasses] ([KlasseId], [CourseID], [KlasseName], [DateStart], [DateEnd]) VALUES (1, 41, N'Background and Colors', CAST(N'2019-08-26' AS Date), CAST(N'2019-08-28' AS Date))
INSERT [dbo].[Klasses] ([KlasseId], [CourseID], [KlasseName], [DateStart], [DateEnd]) VALUES (2, 13, N'Basic sentences', CAST(N'2019-01-28' AS Date), CAST(N'2019-01-30' AS Date))
SET IDENTITY_INSERT [dbo].[Klasses] OFF
SET IDENTITY_INSERT [dbo].[Registerations] ON 

INSERT [dbo].[Registerations] ([RegistrationId], [StudentId], [CourseId]) VALUES (1, 16, 40)
INSERT [dbo].[Registerations] ([RegistrationId], [StudentId], [CourseId]) VALUES (2, 17, 41)
INSERT [dbo].[Registerations] ([RegistrationId], [StudentId], [CourseId]) VALUES (3, 19, 13)
SET IDENTITY_INSERT [dbo].[Registerations] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [Password], [Role], [Birthday], [PhoneNumber], [Street], [City], [PostalCode], [Province], [Photo]) VALUES (13, N'Andree-Ann', N'Charette', N'Naan', N'Teacher', CAST(N'1994-06-17' AS Date), N'4507562964', N'4834 street', N'SheRa', N'J9D0P4', N'Quebec', NULL)
INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [Password], [Role], [Birthday], [PhoneNumber], [Street], [City], [PostalCode], [Province], [Photo]) VALUES (15, N'Josiane', N'Miron', N'Joooo', N'Teacher', CAST(N'1995-05-05' AS Date), N'4508342340', N'7439 street', N'Etheria', N'J7V9S3', N'Quebec', NULL)
INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [Password], [Role], [Birthday], [PhoneNumber], [Street], [City], [PostalCode], [Province], [Photo]) VALUES (16, N'Jessica', N'Michaud', N'Jess', N'Student', CAST(N'1995-09-25' AS Date), N'4509872345', N'8532 street', N'Tigrou', N'J7V9B3', N'Quebec', NULL)
INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [Password], [Role], [Birthday], [PhoneNumber], [Street], [City], [PostalCode], [Province], [Photo]) VALUES (17, N'Adora', N'She-Ra', N'Shera', N'Student', CAST(N'1997-08-10' AS Date), N'5149306395', N'8340 street', N'Etheria', N'J8V0H4', N'Quebec', NULL)
INSERT [dbo].[Users] ([UserId], [FirstName], [LastName], [Password], [Role], [Birthday], [PhoneNumber], [Street], [City], [PostalCode], [Province], [Photo]) VALUES (19, N'Catra', N'cat', N'catra', N'Student', CAST(N'1997-09-07' AS Date), N'5140374784', N'8430 street', N'FrightZone', N'J9V9D8', N'Quebec', NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK_Courses_Users] FOREIGN KEY([TeacherID])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK_Courses_Users]
GO
ALTER TABLE [dbo].[Klasses]  WITH CHECK ADD  CONSTRAINT [FK_Klasses_Courses] FOREIGN KEY([CourseID])
REFERENCES [dbo].[Courses] ([CourseId])
GO
ALTER TABLE [dbo].[Klasses] CHECK CONSTRAINT [FK_Klasses_Courses]
GO
ALTER TABLE [dbo].[Registerations]  WITH CHECK ADD  CONSTRAINT [FK_Registerations_Users] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Registerations] CHECK CONSTRAINT [FK_Registerations_Users]
GO
