﻿using DevExpress.XtraScheduler;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SchoolRegistration
{
    /// <summary>
    /// Interaction logic for StudentCalender.xaml
    /// </summary>
    public partial class StudentCalender : Window
    {
        StudentRegistrationModel context = new StudentRegistrationModel();
        public StudentCalender()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            

            
            
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            this.scheduler.ActiveViewType = DevExpress.Xpf.Scheduling.ViewType.MonthView;
            context.Appointments.Load();
            context.Resources.Load();
            this.scheduler.DataSource.AppointmentsSource = context.Appointments.Local;
            this.scheduler.DataSource.ResourcesSource = context.Resources.Local;
            
        }
    }
}
