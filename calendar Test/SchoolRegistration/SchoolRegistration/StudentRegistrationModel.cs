namespace SchoolRegistration
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    public class StudentRegistrationModel : DbContext
    {
        // Your context has been configured to use a 'StudentRegistrationModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'SchoolRegistration.StudentRegistrationModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'StudentRegistrationModel' 
        // connection string in the application configuration file.
        public StudentRegistrationModel() : base("name=StudentRegistrationModel")
        {
            Database.SetInitializer<StudentRegistrationModel>(new SchedulerDBInitializer());
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }

        public virtual DbSet<EFAppointment> Appointments { get; set; }
        public virtual DbSet<EFResource> Resources { get; set; }
    }
    #region #model
    public class EFAppointment
    {
        [Key()]
        public int UniqueID { get; set; }
        [Required]
        public int Type { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public bool AllDay { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public int Label { get; set; }
        public string ResourceIDs { get; set; }
        public string ReminderInfo { get; set; }
        public string RecurrenceInfo { get; set; }
    }

    public class EFResource
    {
        [Key()]
        public int UniqueID { get; set; }
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public int Color { get; set; }
    }
    #endregion #model
    #region #SchedulerDBInitializer
    public class SchedulerDBInitializer : CreateDatabaseIfNotExists<StudentRegistrationModel>
    {
        protected override void Seed(StudentRegistrationModel context)
        {
            IList<EFResource> defaultResources = new List<EFResource>();

            defaultResources.Add(new EFResource() { ResourceID = 1, ResourceName = "Resource 1" });
            defaultResources.Add(new EFResource() { ResourceID = 2, ResourceName = "Resource 2" });

            foreach (EFResource res in defaultResources)
                context.Resources.Add(res);
            

            
            EFAppointment eFAppointment = new EFAppointment();
            eFAppointment.StartDate = DateTime.Now.Date.AddHours(10);
            eFAppointment.EndDate = DateTime.Now.Date.AddHours(11);
            eFAppointment.AllDay = true;
            eFAppointment.Subject = "Test";
            eFAppointment.Description = "Testing";
            eFAppointment.ResourceIDs = "1";
            eFAppointment.Location = "Test location";


            context.Appointments.Add(eFAppointment);
            base.Seed(context);

        }
    }
    #endregion #SchedulerDBInitializer
}